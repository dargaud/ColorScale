s��         3�     .  �        ����                                           ColorScale                                     �    Color scaling and RGB-HSL conversion functions.
This fp should include ColorScale.h and ColorScale.c

(c) Guillaume Dargaud 1997-2001

http://rome.atmos.colostate.edu/
http://sung3.ifsi.rm.cnr.it/~dargaud/    m    Provides functions to scale between 2 or 3 colors.
You provide 2 colors and a scaling ratio (between 0 and 1) and you get a color ranging between those two, weighted by the ratio.

Variants:
RGB scaling is the default but does not always look good.
Steps are non linear but more visually distinctive.
HSL scaling usually provides a wider range of colors than RGB.     �    Series of functions to convert between RGB (Reg-Green-Blue) representation of colors (usually used on computers) and HSL (Hue-Saturation-Lightness) which is often more useful.     �    Returns the RGB linear interpolated color between 2 colors
            If Ratio=0, you get Col1,
            If Ratio=1, you get Col2

EXAMPLE: Col1=0xFF, Col2=0xFF0000, Ratio=0.5 returns 0x800080
     :    Low color in hex 0xRRGGBB format, returned when Ratio=0
     ;    High color in hex 0xRRGGBB format, returned when Ratio=1
     l    Interpolation ratio between Color1 and Color2

EXAMPLE: Col1=0, Col2=0xFF00FF, Ratio=0.5 returns 0x800080
     5    Returned interpolated color, between 0 and 0xFFFFFF    T $           Color1                            � $ �         Color2                            � t i         Ratio                             M �J��        Scaled color                       0    0    0    	              Returns the HSL linear interpolated color between 2 colors (more natural looking than RGB interpolation).

For instance if the luminance is the same in Col1 and Col2, then the luminance of the result will be the same.

If Ratio=0, you get Col1, If Ratio=1, you get Col2     #    Low color (returned when ratio=0)     $    High color (returned when ratio=1)     i    Scaling between colors (0=Color1, 1=Color2)

EXAMPLE: Col1=0, Col2=0xFF00FF, Ratio=0.5 returns 0x1F5F3F         Returned interpolated color    � $           Color1                            � $ �         Color2                            � j d         Ratio                             X �@��        ScaledColor                        0    0    0    	           �    Similar to ColorScaleRGB, except that it scales the color scaling on a given number of steps.
This method produces more readable color maps (small differences in values are visually enhanced).     :    Low color in hex 0xRRGGBB format, returned when Ratio=0
     ;    High color in hex 0xRRGGBB format, returned when Ratio=1
     l    Interpolation ratio between Color1 and Color2

EXAMPLE: Col1=0, Col2=0xFF00FF, Ratio=0.5 returns 0x800080
     5    Returned interpolated color, between 0 and 0xFFFFFF     �    Number of steps on which to truncate the colors.
=0 returns the same as ColorScaleRGB
=1 returns the opposite of ColorscaleRGB
8~16 useful default values
    
6 $           Color1                            
x $ �         Color2                            
� `          Ratio                             / �J��        Scaled color                      l _ �          NbSteps                            0    	0xFFFFFF    0    	           8    �    Similar to ColorScaleHSL except that it truncates the color scaling on a given number of steps.
This method produces more readable color maps (small differences in values are visually enhanced).     #    Low color (returned when ratio=0)     $    High color (returned when ratio=1)     i    Scaling between colors (0=Color1, 1=Color2)

EXAMPLE: Col1=0, Col2=0xFF00FF, Ratio=0.5 returns 0x1F5F3F         Returned interpolated color     �    Number of steps on which to truncate the colors.
=0 returns the same as ColorScaleHSL
=1 returns the opposite of ColorscaleHSL
8~16 useful default value
     $           Color1                            7 $ �         Color2                            c j          Ratio                             � �@��        ScaledColor                       � p �          NbSteps                            0    0    0    	           8   �    Returns the RGB linear interpolated color between 3 colors

Ratio1/2/3 is the weight assigned to color 1/2/3.
You may pass any value for Ratio1/2/3 as the resulting color is averaged with those weights.

In general you want to pass Ratio1+Ratio2+Ratio3=1 or Ratio1*Ratio1+Ratio2*Ratio2+Ratio3*Ratio3=1 making sure that they stay between 0 and 1.

Ratio1=Ratio2=Ratio3=0 is undefined (will return Color1)     8    First color in hex 0xRRGGBB format, weighted by Ratio1     :    Second color in hex 0xRRGGBB format, weighted by Ratio2
     2    Weight ratio of Color1
0=no weight
1=max weight
     5    Returned interpolated color, between 0 and 0xFFFFFF     9    Third color in hex 0xRRGGBB format, weighted by Ratio3
     2    Weight ratio of Color2
0=no weight
1=max weight
     2    Weight ratio of Color3
0=no weight
1=max weight
    b $           Color1                            � $ �         Color2                            � p          Ratio1                             �J��        Scaled color                      [ $+         Color3                            � p �         Ratio2                            � p+         Ratio3                             0    0    0    	           0    0    0   �    Returns the HSL linear interpolated color between 3 colors

Ratio1/2/3 is the weight assigned to color 1/2/3.
You may pass any value for Ratio1/2/3 as the resulting color is averaged with those weights.

In general you want to pass Ratio1+Ratio2+Ratio3=1 or Ratio1*Ratio1+Ratio2*Ratio2+Ratio3*Ratio3=1 making sure that they stay between 0 and 1.

Ratio1=Ratio2=Ratio3=0 is undefined (will return Color1)     8    First color in hex 0xRRGGBB format, weighted by Ratio1     :    Second color in hex 0xRRGGBB format, weighted by Ratio2
     2    Weight ratio of Color1
0=no weight
1=max weight
     5    Returned interpolated color, between 0 and 0xFFFFFF     9    Third color in hex 0xRRGGBB format, weighted by Ratio3
     2    Weight ratio of Color2
0=no weight
1=max weight
     2    Weight ratio of Color3
0=no weight
1=max weight
    J $           Color1                            � $ �         Color2                            � p          Ratio1                             �J��        Scaled color                      C $+         Color3                            � p �         Ratio2                            � p+         Ratio3                             0    0    0    	           0    0    0    1    MACRO returning a color darker by 0.1 lightness     !    Color to change (0 to 0xFFFFFF)    � $           Color                           ���� ~���        Return                             0    	           1    MACRO returning a color darker by 0.2 lightness     !    Color to change (0 to 0xFFFFFF)    � $           Color                           ���� t���        Return                             0    	           2    MACRO returning a color lighter by 0.1 lightness     !    Color to change (0 to 0xFFFFFF)    � $           Color                           ���� `���        Return                             0    	           2    MACRO returning a color lighter by 0.2 lightness     !    Color to change (0 to 0xFFFFFF)    g $           Color                           ���� `���        Return                             0    	           :    MACRO returning a color more saturated by 0.1 saturation     !    Color to change (0 to 0xFFFFFF)    M $           Color                           ���� ~���        Return                             0    	           :    MACRO returning a color more saturated by 0.2 saturation     !    Color to change (0 to 0xFFFFFF)    3 $           Color                           ���� t���        Return                             0    	           :    MACRO returning a color less saturated by 0.1 saturation     !    Color to change (0 to 0xFFFFFF)      $           Color                           ���� `���        Return                             0    	           :    MACRO returning a color less saturated by 0.2 saturation     !    Color to change (0 to 0xFFFFFF)     � $           Color                           ���� `���        Return                             0    	           4    MACRO returning a color with a hue changed by +0.1     !    Color to change (0 to 0xFFFFFF)    !� $           Color                           ���� ~���        Return                             0    	           4    MACRO returning a color with a hue changed by +0.2     !    Color to change (0 to 0xFFFFFF)    "� $           Color                           ���� t���        Return                             0    	           2    MACRO returning a color lighter by 0.1 lightness     4    MACRO returning a color with a hue changed by -0.1    #� $           Color                           ���� `���        Return                             0    	           4    MACRO returning a color with a hue changed by -0.2     !    Color to change (0 to 0xFFFFFF)    $� $           Color                           ���� `���        Return                             0    	               Convert from RGB to HSL     *    Hex RGB color to convert (0 to 0xFFFFFF)         Returned Hue (0 to 1)         Returned Saturation (0 to 1)         Returned Lightness (0 to 1)    %U $           RGB                               %� `          Hue                               %� `         Saturation                        %� ` �         Lightness                          0    	           	           	               Convert color from HSL to RGB     #    Input Hue of the color to convert     *    Input saturation of the color to convert     )    Input Lightness of the color to convert     $    Returned RGB color (0 to 0xFFFFFF)    ' $           Hue                               '@ #         Saturation                        'r # �         Lightness                         '� �^��        RGB                                0    0    0    	           �    Make a color from separate components

Example:
MakeRGB(0x10, 0, 0xA0) will return 0x1000A0

Note, this is a macro with error checking         Red component (0 to 255)         Green component (0 to 255)         Blue component (0 to 255)          Returned color (0 to 0xFFFFFF)    )N )           Red                               )p ) �         Green                             )� )         Blue                              )� mj��        RGB color                          0    0    0    	           �    Make a color from separate components

Example:
MakeRGBA(0x10, 0, 0xA0, 1) will return 0x11000A0

Note, this is a macro with error checking         Red component (0 to 255)         Green component (0 to 255)         Blue component (0 to 255)          Returned color (0 to 0xFFFFFF)     �    Alpha Channel component (0 to 255).

This is used for instance to produce semi-transparent colors.
The LabWindows interface considers 1 as a transparent value.
If you save to 32 bit PNG file (See ReadSavePng) you can use the full range of values.    +c *           Red                               +� * �         Green                             +� *         Blue                              +� mj��        RGBA color                        +� *z         Alpha                              0    0    0    	           0 ����         �  �     G.    ColorScaleRGB                   ����         y  }     K.    ColorScaleHSL                   ����         	l       G.    ColorStepsRGB                   ����         @  �     K.    ColorStepsHSL                   ����         �       G.    ColorScaleRGB3                  ����         �  �     G.    ColorScaleHSL3                  ����         �  �     K.    DARKER                          ����         r  �     K.    DARKEST                         ����         O  �     K.    LIGHTER                         ����         -  �     K.    LIGHTEST                        ����           v     K.    MORE_SATURATED                  ����         �  \     K.    MOST_SATURATED                  ����         �   B     K.    LESS_SATURATED                  ����          �  !(     K.    LEAST_SATURATED                 ����         !�  "     K.    MORE_HUED                       ����         "�  "�     K.    MOST_HUED                       ����         #c  #�     K.    LESS_HUED                       ����         $T  $�     K.    LEAST_HUED                      ����         %4  %�     K.    RGBtoHLS                        ����         &�  '�     K.    HLStoRGB                        ����         (�  )�     G*    MakeRGB                         ����         *�  ,�     G*    MakeRGBA                               �                                    WColor Scaling                        DRGB scale between colors             DHSL scale between colors             DRGB steps between colors             DHSL steps between colors             DRGB scale between 3 colors           DHSL scale between 3 colors        ����Single Color modification            DDarker color                         DMuch darker color                    DLighter color                        DMuch lighter color                   DMore saturated color                 DMost saturated color                 DLess saturated color                 DLeast saturated color                DMore hued color                      DMost hued color                      DLess hued color                      DLeast hued color                    �Color Conversion                     DColor conversion RGB to HLS          DColor conversion HLS to RGB          DMake color from separate R G B       DMake color from separate RGB+A  