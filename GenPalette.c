/////////////////////////////////////////////////////////////////////////////////////
// PROGRAM:   GenPalette.exe                                                       //
// COPYRIGHT: Guillaume Dargaud 1999-2002 - Freeware                               //
// PURPOSE:   Create palette files                                                 //
// INSTALL:   run SETUP. It will also install the LabWindows/CVI runtime engine.   //
// HELP:      available by right-clicking an item.                                 //
// TUTORIAL:  http://www.gdargaud.net/Hack/GenPalette.html                         //
/////////////////////////////////////////////////////////////////////////////////////

#define _VER_ "1.3"			// Version number
#define _WEB_ "http://www.gdargaud.net/Hack/GenPalette.html"
#define _COPY_ "� 1999-2004 Guillaume Dargaud"

#include <stdio.h>
#include <string.h>
#include <iso646.h>

#include <utility.h>
#include <formatio.h>
#include <userint.h>
#include <cvirte.h>    /* Needed if linking in external compiler; harmless otherwise */

#include "def.h"
#include "ColorScale.h"

#include "GenPalette.h"

static int Panel=0, PnlT=0, Menu=0;

static COLORREF Color, Col1=0, Col2=0, *ColArray=NULL;
static int H1=0, H2=0, Dim=0;
static int CanvasRef=0, CanvasRefTop, CanvasRefLeft;
static char Dir[MAX_PATHNAME_LEN]="";

///////////////////////////////////////////////////////////////////////////////
static void ResetPalette(void) {
	Free(ColArray); ColArray=NULL;
	CanvasClear (Panel, PANEL_CANVAS, VAL_ENTIRE_OBJECT);
	Dim=H1=H2=0;
	GetCtrlVal(Panel, PANEL_COLOR, &Col1);
	Col2=Col1;
	SetMenuBarAttribute (Menu, MENU_FILE_SAVE_PAL, ATTR_DIMMED, TRUE);
	SetMenuBarAttribute (Menu, MENU_FILE_SAVE_TXT, ATTR_DIMMED, TRUE);
	SetMenuBarAttribute (Menu, MENU_FILE_SAVE_PNG, ATTR_DIMMED, TRUE);
}

static void GeneratePalette(void) {
	int Height, Width, i, Hsl, File;
	COLORREF C, (*Scale)(const COLORREF, const COLORREF, const double);
	Point P1, P2;
	char Str[100];

	GetCtrlAttribute (Panel, PANEL_CANVAS, ATTR_HEIGHT, &Height);
	GetCtrlAttribute (Panel, PANEL_CANVAS, ATTR_WIDTH,  &Width);
	GetCtrlVal(Panel, PANEL_HSL_RGB, &Hsl);
	if (Hsl) Scale=ColorScaleHSL; else Scale=ColorScaleRGB;

	if (ColArray==NULL) ColArray=Calloc(Dim=Height, COLORREF);
//	for (i=0; i<Dim; i++) ColArray[i]=0xffffff;

	SetMenuBarAttribute (Menu, MENU_FILE_SAVE_PAL, ATTR_DIMMED, FALSE);
	SetMenuBarAttribute (Menu, MENU_FILE_SAVE_TXT, ATTR_DIMMED, FALSE);
	SetMenuBarAttribute (Menu, MENU_FILE_SAVE_PNG, ATTR_DIMMED, FALSE);

	P1.x=0;	P2.x=Width-1;
	CanvasStartBatchDraw (Panel, PANEL_CANVAS);
	for (i=H1; i<=H2; i++) {
		P1.y=P2.y=i;
		SetCtrlAttribute(Panel, PANEL_CANVAS, ATTR_PEN_COLOR,
			ColArray[i]=Scale(Col1, Col2, (double)(i-H1)/(H2-H1-1)));
		CanvasDrawLine (Panel, PANEL_CANVAS, P1, P2);
	}
	CanvasEndBatchDraw (Panel, PANEL_CANVAS);
	CanvasUpdate (Panel, PANEL_CANVAS, VAL_ENTIRE_OBJECT);
}


///////////////////////////////////////////////////////////////////////////////
int main (int argc, char *argv[]) {
	if (InitCVIRTE (0, argv, 0) == 0)	/* Needed if linking in external compiler; harmless otherwise */
		return -1;	/* out of memory */
	if ((Panel = LoadPanel (0, "GenPalette.uir", PANEL)) < 0 or
		(PnlT = LoadPanel (0, "GenPalette.uir", PNLT)) <0 ) {
		MessagePopup("Error", "File GenPalette.uir missing or corrupt !");
		return -1;
	}
	Menu = GetPanelMenuBar (Panel);
	DisplayPanel (Panel);
	CanvasRef=PANEL_TEMP_REF;
	cb_Panel(Panel, EVENT_PANEL_SIZE, NULL, 0, 0);
	ResetPalette();
	RunUserInterface ();
	if (ColArray!=NULL) free(ColArray); ColArray=NULL;
	return 0;
}


///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Color (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(Panel, PANEL_COLOR, &Color);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Color",
						 "Color to set on the canvas.\n"
						 "After selecting a new color, click on the canvas to create a palette.\n"
						 "Note 1: when you press [Reset], this will be the color used for starter.\n"
						 "Note 2: You can click on the reference canvas to set.");
			break;
	}
	return 0;
}

int CVICALLBACK cb_Quit (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			QuitUserInterface (0);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Quit", "What did you seriously expect to see here ?!?");
			break;
	}
	return 0;
}

int CVICALLBACK cb_Canvas (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int Top;
	switch (event) {
		case EVENT_LEFT_CLICK:
		case EVENT_COMMIT:
			GetCtrlAttribute (Panel, PANEL_CANVAS, ATTR_TOP, &Top);
			H1=H2;
			Col1=Col2;
			Col2=Color;
			H2=eventData1-Top;
			GeneratePalette();
//			MoveInFront (Panel, PANEL_CANVAS, PANEL_COPY2REF);
//			MoveInFront (Panel, CanvasRef, PANEL_COPY2REF);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Canvas",
						 "That's where the results of the interpolations are drawn.\n"
						 "Select a first color and press [Reset].\n"
						 "Then select a new color and click on the canvas:\n"
						 "An interpolation of colors is draw between the top\n"
						 "and the place you clicked.\n"
						 "Continue by selecting a new color and clicking below\n"
						 "the last clicked point of the canvas.\n"
						 "To conclude: select a last color and click [Last].\n\n"
						 "You can then copy the canvas to the reference [>>]\n"
						 "if you want to redraw it a little better.");
			break;
	}
	return 0;
}

int CVICALLBACK cb_Panel (int panel, int event, void *callbackData,
		int eventData1, int eventData2) {
	int PnlWidth, PnlHeight, Top, Left, CtrlHeight, CtrlWidth, CanW;
	static int LastWidth=0, LastHeight=0;
	switch (event) {
		case EVENT_CLOSE:
			QuitUserInterface (0);
			break;

		case EVENT_PANEL_SIZE:
			GetPanelAttribute (Panel, ATTR_WIDTH,  &PnlWidth);
			GetPanelAttribute (Panel, ATTR_HEIGHT, &PnlHeight);
			if (PnlWidth==LastWidth and PnlHeight==LastHeight) return 0;
			LastWidth=PnlWidth;
			LastHeight=PnlHeight;
			if (PnlWidth<160) PnlWidth=160;
			if (PnlHeight<20) PnlHeight=20;

			GetCtrlAttribute(Panel, PANEL_CANVAS, ATTR_TOP,  &Top);
			GetCtrlAttribute(Panel, PANEL_CANVAS, ATTR_LEFT, &Left);
			GetCtrlAttribute(Panel, PANEL_LAST,   ATTR_HEIGHT, &CtrlHeight);
			SetCtrlVal      (Panel, PANEL_FIX_HEIGHT, PnlHeight-Top-CtrlHeight);

			SetCtrlAttribute(Panel, PANEL_CANVAS, ATTR_WIDTH, CanW=(PnlWidth-Left)/2);
			SetCtrlAttribute(Panel, PANEL_CANVAS, ATTR_HEIGHT, PnlHeight-Top-CtrlHeight);

			SetCtrlAttribute(Panel, CanvasRef, ATTR_TOP, CanvasRefTop=Top);
			SetCtrlAttribute(Panel, CanvasRef, ATTR_LEFT, CanvasRefLeft=Left+CanW+1);
			SetCtrlAttribute(Panel, CanvasRef, ATTR_WIDTH, CanW-1);
			SetCtrlAttribute(Panel, CanvasRef, ATTR_HEIGHT, PnlHeight-Top-CtrlHeight);

			GetCtrlAttribute(Panel, PANEL_LAST, ATTR_HEIGHT, &CtrlHeight);
			SetCtrlAttribute(Panel, PANEL_LAST, ATTR_TOP,    PnlHeight-CtrlHeight);
			GetCtrlAttribute(Panel, PANEL_COPYRIGHT, ATTR_HEIGHT, &CtrlHeight);
			SetCtrlAttribute(Panel, PANEL_COPYRIGHT, ATTR_TOP,    PnlHeight-CtrlHeight);
			GetCtrlAttribute(Panel, PANEL_COPY2REF, ATTR_HEIGHT, &CtrlHeight);
			SetCtrlAttribute(Panel, PANEL_COPY2REF, ATTR_TOP,    PnlHeight-CtrlHeight);
			GetCtrlAttribute(Panel, PANEL_COPY2REF, ATTR_WIDTH, &CtrlWidth);
			SetCtrlAttribute(Panel, PANEL_COPY2REF, ATTR_LEFT,  Left+CanW-CtrlWidth/2);
			ResetPalette();
			break;
	}
	return 0;
}


///////////////////////////////////////////////////////////////////////////////
void CVICALLBACK cbm_SavePal(int menuBar, int menuItem, void *callbackData, int panel) {
	int File, pos, i, Sel;
	COLORREF C;
	static char PathName[MAX_PATHNAME_LEN]="", Str[100];
	char Dir2[MAX_DIRNAME_LEN];

	if (ColArray==NULL) return;
	Sel = FileSelectPopup (Dir, "*.pal", "*.pal", "Save to .pal (PSP compatible file)",
						   VAL_SAVE_BUTTON, 0, 1, 1, 1, PathName);
	if (Sel<=0) return;
	File = OpenFile (PathName, VAL_WRITE_ONLY, VAL_TRUNCATE, VAL_ASCII);
	WriteLine (File, "JASC-PAL\n0100\n256", -1);
	for (i=0; i<256; i++) {
		pos=i*Dim/256;
		C=ColArray[i];
		sprintf(Str, "%d %d %d", C>>16, (C>>8)&0xFF, C&0xFF);
		WriteLine (File, Str, -1);
	}
	CloseFile (File);
	SplitPath (PathName, Dir, Dir2, NULL);
	strcat(Dir, Dir2);
}


void CVICALLBACK cbm_SaveTxt(int menuBar, int menuItem, void *callbackData, int panel) {
	int File, pos, i, Sel, TxtFormat;
	COLORREF C;
	double H, S, L;
	static char PathName[MAX_PATHNAME_LEN]="", Str[100];
	char Dir2[MAX_DIRNAME_LEN];

	if (ColArray==NULL) return;
	Sel = FileSelectPopup (Dir, "*.txt", "*.txt",
						   "Save to text file", VAL_SAVE_BUTTON, 0, 0, 1,
						   1, PathName);
	if (Sel<=0) return;

	GetCtrlVal(PnlT, PNLT_TXT_FORMAT, &TxtFormat);
	File = OpenFile ("Palette.txt", VAL_WRITE_ONLY, VAL_TRUNCATE, VAL_ASCII);
	for (i=0; i<Dim; i++) {
		C=ColArray[i];
		switch (TxtFormat) {
			case 0: sprintf(Str, "%d", C); break;
			case 1: sprintf(Str, "%06X", C); break;
			case 2: sprintf(Str, "#%06X", C); break;
			case 3: sprintf(Str, "0x%06X", C); break;
			case 4: sprintf(Str, "%d %d %d", C>>16, (C>>8)&0xFF, C&0xFF); break;
			case 5: sprintf(Str, "0x%02X 0x%02X 0x%02X", C>>16, (C>>8)&0xFF, C&0xFF); break;
			case 6: RGBtoHLS (C, &H, &L, &S);
					sprintf(Str, "%.5f %.5f %.5f", H, S, L); break;
			case 7: RGBtoHLS (C, &H, &L, &S);
					sprintf(Str, "0x%02x 0x%02x 0x%02x", (int)(H*255+.5), (int)(S*255+.5), (int)(L*255+.5)); break;
		}
		WriteLine(File, Str, -1);
	}
	CloseFile(File);
	SplitPath (PathName, Dir, Dir2, NULL);
	strcat(Dir, Dir2);
}

void Png_SavePanelControlToFile (int Pnl, int Control, char* PathName) {
	int Bitmap;
	GetCtrlBitmap (Pnl, Control, 0, &Bitmap);
	SaveBitmapToPNGFile(Bitmap, PathName);
	DiscardBitmap (Bitmap);
}


void CVICALLBACK cbm_SavePng(int menuBar, int menuItem, void *callbackData, int panel) {
	int File, pos, i, Sel;
	COLORREF C;
	static char PathName[MAX_PATHNAME_LEN]="", Str[100];
	char Dir2[MAX_DIRNAME_LEN];

	if (ColArray==NULL) return;
	Sel = FileSelectPopup (Dir, "*.png", "*.png", "Save canvas to PNG file",
						   VAL_SAVE_BUTTON, 0, 1, 1, 1, PathName);
	if (Sel<=0) return;
	Png_SavePanelControlToFile (Panel, PANEL_CANVAS, PathName);
	SplitPath (PathName, Dir, Dir2, NULL);
	strcat(Dir, Dir2);
}

void CVICALLBACK cbm_SaveRefPng(int menuBar, int menuItem, void *callbackData, int panel) {
	int File, pos, i, Sel;
	COLORREF C;
	static char PathName[MAX_PATHNAME_LEN]="";
	char Dir2[MAX_DIRNAME_LEN];

	if (ColArray==NULL) return;
	Sel = FileSelectPopup (Dir, "*.png", "*.png", "Save canvas to PNG file",
						   VAL_SAVE_BUTTON, 0, 1, 1, 1, PathName);
	if (Sel<=0) return;
	Png_SavePanelControlToFile (Panel, PANEL_CANVAS, PathName);
	SplitPath (PathName, Dir, Dir2, NULL);
	strcat(Dir, Dir2);
}


///////////////////////////////////////////////////////////////////////////////
void CVICALLBACK cbm_LoadPal(int menuBar, int menuItem, void *callbackData, int panel) {
	int File, pos, i, Sel, R, G, B, Width;
	COLORREF C;
	Point P1, P2;
	#define DIM 100
	static char PathName[MAX_PATHNAME_LEN]="", Str[DIM+1];
	char Dir2[MAX_DIRNAME_LEN];


	Sel = FileSelectPopup (Dir, "*.pal", "*.pal", "Load .pal file",
						   VAL_LOAD_BUTTON, 0, 1, 1, 0, PathName);
	if (Sel!=VAL_EXISTING_FILE_SELECTED) return;
	cb_256(Panel, PANEL_256, EVENT_COMMIT, NULL, 0, 0);
	if (ColArray!=NULL) free(ColArray); ColArray=NULL;
	ColArray=Calloc(Dim=256, COLORREF);

	File = OpenFile (PathName, VAL_READ_ONLY, VAL_OPEN_AS_IS, VAL_ASCII);
	ReadLine (File, Str, DIM);	// JASC-PAL
	ReadLine (File, Str, DIM);	// 0100
	ReadLine (File, Str, DIM);	// 256
	if (atoi(Str)!=256) {
		MessagePopup("Error", "Invalid palette file.\n"
				"It must contain 256 entries of the form R G B on each line\n"
				"with R/G/B between 0 and 255.");
		goto End;
	}

	GetCtrlAttribute(Panel, PANEL_CANVAS, ATTR_WIDTH, &Width);
	P1.x=0;	P2.x=Width-1;
	CanvasStartBatchDraw (Panel, PANEL_CANVAS);
	for (i=0; i<256; i++) {
		P1.y=P2.y=i;
		ReadLine (File, Str, -1);
//		pos=i*Dim/256;
		sscanf(Str, "%d %d %d", &R, &G, &B);
		ColArray[i]=MakeColor (R, G, B);
		SetCtrlAttribute(Panel, PANEL_CANVAS, ATTR_PEN_COLOR, ColArray[i]);
		CanvasDrawLine (Panel, PANEL_CANVAS, P1, P2);
	}
	CanvasEndBatchDraw (Panel, PANEL_CANVAS);
	CanvasUpdate (Panel, PANEL_CANVAS, VAL_ENTIRE_OBJECT);

End:
	CloseFile (File);
	SplitPath (PathName, Dir, Dir2, NULL);
	strcat(Dir, Dir2);
}


///////////////////////////////////////////////////////////////////////////////
void CVICALLBACK cbm_LoadTxt(int menuBar, int menuItem, void *callbackData, int panel) {
	int File, pos, i, Sel, R, G, B, H, S, L, Width, TxtFormat, D;
	COLORREF C=0;
	Point P1, P2;
	static char PathName[MAX_PATHNAME_LEN]="", Str[DIM+1];
	char Dir2[MAX_DIRNAME_LEN];


	Sel = FileSelectPopup (Dir, "*.txt", "*.txt", "Load .txt file",
						   VAL_LOAD_BUTTON, 0, 0, 1, 0, PathName);
	if (Sel!=VAL_EXISTING_FILE_SELECTED) return;

// Count lines
	D=0;
	File = OpenFile (PathName, VAL_READ_ONLY, VAL_OPEN_AS_IS, VAL_ASCII);
	while (0<=ReadLine (File, Str, DIM)) D++;
	CloseFile (File);

	SetCtrlVal(Panel, PANEL_FIX_HEIGHT, D);
	cb_FixHeight(Panel, PANEL_FIX_HEIGHT, EVENT_COMMIT, NULL, 0, 0);
	if (ColArray!=NULL) free(ColArray); ColArray=NULL;
	ColArray=Calloc(Dim=D, COLORREF);

	File = OpenFile (PathName, VAL_READ_ONLY, VAL_OPEN_AS_IS, VAL_ASCII);

	GetCtrlVal(PnlT, PNLT_TXT_FORMAT, &TxtFormat);
	GetCtrlAttribute(Panel, PANEL_CANVAS, ATTR_WIDTH, &Width);
	P1.x=0;	P2.x=Width-1;
	CanvasStartBatchDraw (Panel, PANEL_CANVAS);
	for (i=0; i<Dim; i++) {
		P1.y=P2.y=i;
		ReadLine (File, Str, -1);
//		pos=i*Dim/256;
		switch (TxtFormat) {
			case 0: sscanf(Str, "%d", &C); break;
			case 1: sscanf(Str, "%06X", &C); break;
			case 2: sscanf(Str, "#%06X", &C); break;
			case 3: sscanf(Str, "0x%06X", &C); break;
			case 4: sscanf(Str, "%d %d %d", &R, &G, &B);
					C=MakeColor (R, G, B); break;
			case 5: sscanf(Str, "0x%02X 0x%02X 0x%02X", &R, &G, &B);
					C=MakeColor (R, G, B); break;
			case 6: sprintf(Str, "%.5f %.5f %.5f", &H, &S, &L);
					C=HLStoRGB (H/255., L/255., S/255.); break;
			case 7: sprintf(Str, "0x%02x 0x%02x 0x%02x", &H, &S, &L);
					C=HLStoRGB (H/255., L/255., S/255.); break;
		}
		ColArray[i]=C;
		SetCtrlAttribute(Panel, PANEL_CANVAS, ATTR_PEN_COLOR, ColArray[i]);
		CanvasDrawLine (Panel, PANEL_CANVAS, P1, P2);
	}
	CanvasEndBatchDraw (Panel, PANEL_CANVAS);
	CanvasUpdate (Panel, PANEL_CANVAS, VAL_ENTIRE_OBJECT);

End:
	CloseFile (File);
	SplitPath (PathName, Dir, Dir2, NULL);
	strcat(Dir, Dir2);
}


///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_256 (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int Height, Top, Width;
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlAttribute(Panel, PANEL_LAST, ATTR_HEIGHT, &Height);
			GetCtrlAttribute(Panel, PANEL_CANVAS, ATTR_TOP, &Top);
			SetPanelAttribute(Panel, ATTR_HEIGHT, 256+Top+Height);
			cb_Panel(Panel, EVENT_PANEL_SIZE, NULL, 0, 0);
//			GetPanelAttribute(Panel, ATTR_WIDTH, &Width);
//			SetPanelSize (Panel, 256+Top+Height, Width);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: 256",
						 "Set the window height to 256 lines,\n"
						 "so as to produce a standard 256 color palette.");
			break;
	}
	return 0;
}

int CVICALLBACK cb_FixHeight (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int Height, Top, Fix;
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(Panel, PANEL_FIX_HEIGHT, &Fix);
			GetCtrlAttribute(Panel, PANEL_LAST, ATTR_HEIGHT, &Height);
			GetCtrlAttribute(Panel, PANEL_CANVAS, ATTR_TOP, &Top);
			SetPanelAttribute(Panel, ATTR_HEIGHT, Fix+Top+Height);
			cb_Panel(Panel, EVENT_PANEL_SIZE, NULL, 0, 0);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Nb",
						 "Set the window height which is also\n"
						 "the number of colors in the palette.");
			break;
	}
	return 0;
}

int CVICALLBACK cb_Reset (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			ResetPalette();
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Reset",
						 "Set the cursor position on the canvas to the top.\n"
						 "Use the current color as a reference for the next interpolation.");
			break;
	}
	return 0;
}

int CVICALLBACK cb_Last (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int Height, Top;
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlAttribute (Panel, PANEL_CANVAS, ATTR_HEIGHT, &Height);
			GetCtrlAttribute (Panel, PANEL_CANVAS, ATTR_TOP, &Top);
			cb_Canvas (Panel, PANEL_CANVAS, EVENT_COMMIT, NULL, Top+Height-1, 0);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Last",
						 "Fills the canvas to its end with the selected color.");
			break;
	}
	return 0;
}


int CVICALLBACK cb_CopyToRef (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			DiscardCtrl (Panel, CanvasRef);
			CanvasRef = DuplicateCtrl (Panel, PANEL_CANVAS, Panel, "Ref", CanvasRefTop, CanvasRefLeft);
			SetCtrlAttribute (Panel, CanvasRef, ATTR_CALLBACK_FUNCTION_POINTER, cb_CanvasRef);
//			MoveInFront (Panel, PANEL_CANVAS, PANEL_COPY2REF);
//			MoveInFront (Panel, CanvasRef, PANEL_COPY2REF);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Copy to Ref",
						 "Copy the current Canvas to the Reference canvas.\n"
						 "You can then pick colors from the Reference canvas\n"
						 "simply by clicking on it.");
			break;
	}
	return 0;
}

int CVICALLBACK cb_CanvasRef (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int Top;
	Point P;
	switch (event) {
		case EVENT_LEFT_CLICK:
		case EVENT_COMMIT:
			GetCtrlAttribute (Panel, CanvasRef, ATTR_TOP, &Top);
			P.y=eventData1-Top;
			P.x=0;
			CanvasGetPixel (Panel, CanvasRef, P, &Color);
			SetCtrlVal(Panel, PANEL_COLOR, Color);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Reference",
						 "This canvas can be copied from the interpolation canvas\n"
						 "by pressing [>>].\n"
						 "You can then pick colors from the reference simply by clicking on it.");
			break;
	}
	return 0;
}

int CVICALLBACK cb_Scaling (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:

			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Scaling",
						 "Controls the way the interpolation between the 2 colors is done.\n"
						 "If set to RGB, then it is done in a 3D space of axes Red, Green and Blue.\n"
						 "If set to HSL, then it is done in a 3D space of axes Hue, Saturation, lightness.");
			break;
	}
	return 0;
}

int CVICALLBACK cb_TxtFormat (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			RemovePopup (0);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Text Format",
						 "Format to use for writing the colors to the text file.\n"
						 "The file PALETTE.TXT is written in the program directory\n"
						 "each time the palette is modified on-screen.\n"
						 "Most major color format are supported:\n"
						 "Decimal or hex RGB, separated RGB channels, HSL...");
			break;
	}
	return 0;
}


///////////////////////////////////////////////////////////////////////////////
void CVICALLBACK cbm_QuickHelp (int menuBar, int menuItem, void *callbackData, int panel) {
	MessagePopup("GenPalette Quick Help",
				"This program allows you to quickly create palette files.\n"
				"1- Select a color and press [Reset]\n"
				"2- Select a color and click on the unused portion of the canvas\n"
				"3- Repeat the previous step, possibly changing the scaling between RGB and HSL\n"
				"4- select a last color and click [Last]\n"
				"\n"
				"Use [>>] to make a copy of the canvas from which you can pick colors.\n"
				"Save as a Paint Shop Pro .pal file (256 colors only) or as text file.\n"
				"Use [256] to force the number of colors to the standard 256.\n"
				"The text file has the same number of colors than the vertical lines in the canvas.");
}

void CVICALLBACK cbm_Website (int menuBar, int menuItem, void *callbackData, int panel) {
	LaunchExecutable ("EXPLORER.EXE "_WEB_);
}

void CVICALLBACK cbm_About (int menuBar, int menuItem, void *callbackData, int panel) {
	MessagePopup("About GenPalette",
				"GenPalette.exe version " _VER_ "\n"
				"Freeware "_COPY_"\n"
				"Last compiled on " __DATE__ "\n"
				"More information and tutorial at "_WEB_);
}


///////////////////////////////////////////////////////////////////////////////
void CVICALLBACK cbm_CopyCanvas(int menuBar, int menuItem, void *callbackData, int panel) {
	int Bitmap;
	GetCtrlBitmap (Panel, PANEL_CANVAS, 0, &Bitmap);
	ClipboardPutBitmap (Bitmap);
	DiscardBitmap (Bitmap);
}

void CVICALLBACK cbm_CopyRef(int menuBar, int menuItem, void *callbackData, int panel) {
	int Bitmap;
	GetCtrlBitmap (Panel, PANEL_TEMP_REF, 0, &Bitmap);
	ClipboardPutBitmap (Bitmap);
	DiscardBitmap (Bitmap);
}

void CVICALLBACK cbm_Quit(int menuBar, int menuItem, void *callbackData, int panel) {
	QuitUserInterface (0);
}

void CVICALLBACK cbm_SetTxt(int menuBar, int menuItem, void *callbackData, int panel) {
	InstallPopup (PnlT);
}
