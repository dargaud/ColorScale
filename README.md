PROGRAMS:   GenPalette.exe  
            ColorInterim.exe                                                        

VERSION:    1.4 and 1.0

AUTHOR:     Guillaume Dargaud                                 

PURPOSE:    Generation of color palettes based on RGB or HSL interpolation between given colors.

INSTALL:    run SETUP.  
            If setup fails, you may need to install the LabWindows/CVI runtime engine on your PC.  
            It is available from the National Instrument website http://www.ni.com/

HELP:       available by right-clicking an item on the user interface.

TUTORIAL:   http://www.gdargaud.net/Hack/GenPalette.html

LICENSE:   GPLv2
