/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 1997. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PNL                             1
#define  PNL_AFT2                        2
#define  PNL_BEF2                        3
#define  PNL_AFT1                        4
#define  PNL_BEF1                        5
#define  PNL_GRAPH                       6
#define  PNL_RGB2                        7
#define  PNL_RGB1                        8
#define  PNL_L2                          9       /* callback function: CB_HSL2 */
#define  PNL_S2                          10      /* callback function: CB_HSL2 */
#define  PNL_H2                          11      /* callback function: CB_HSL2 */
#define  PNL_B2                          12      /* callback function: CB_RGB2 */
#define  PNL_G2                          13      /* callback function: CB_RGB2 */
#define  PNL_R2                          14      /* callback function: CB_RGB2 */
#define  PNL_L1                          15      /* callback function: CB_HSL1 */
#define  PNL_S1                          16      /* callback function: CB_HSL1 */
#define  PNL_H1                          17      /* callback function: CB_HSL1 */
#define  PNL_B1                          18      /* callback function: CB_RGB1 */
#define  PNL_G1                          19      /* callback function: CB_RGB1 */
#define  PNL_R1                          20      /* callback function: CB_RGB1 */
#define  PNL_QUIT                        21      /* callback function: cb_Quit */
#define  PNL_SCALE                       22      /* callback function: cb_Scale */


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */ 

int  CVICALLBACK CB_HSL1(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_HSL2(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Quit(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_RGB1(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK CB_RGB2(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Scale(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
