/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
	extern "C" {
#endif

	 /* Panels and Controls: */

#define  PANEL                            1       /* callback function: cb_Panel */
#define  PANEL_FIX_HEIGHT                 2       /* control type: numeric, callback function: cb_FixHeight */
#define  PANEL_256                        3       /* control type: command, callback function: cb_256 */
#define  PANEL_COLOR                      4       /* control type: color, callback function: cb_Color */
#define  PANEL_HSL_RGB                    5       /* control type: binary, callback function: cb_Scaling */
#define  PANEL_TXT_USE                    6       /* control type: textMsg, callback function: (none) */
#define  PANEL_RESET                      7       /* control type: command, callback function: cb_Reset */
#define  PANEL_CANVAS                     8       /* control type: canvas, callback function: cb_Canvas */
#define  PANEL_LAST                       9       /* control type: command, callback function: cb_Last */
#define  PANEL_COPY2REF                   10      /* control type: command, callback function: cb_CopyToRef */
#define  PANEL_COPYRIGHT                  11      /* control type: textMsg, callback function: (none) */
#define  PANEL_TEMP_REF                   12      /* control type: canvas, callback function: cb_CanvasRef */
#define  PANEL_QUIT                       13      /* control type: command, callback function: cb_Quit */

#define  PNLT                             2
#define  PNLT_TXT_FORMAT                  2       /* control type: slide, callback function: cb_TxtFormat */


	 /* Control Arrays: */

		  /* (no control arrays in the resource file) */


	 /* Menu Bars, Menus, and Menu Items: */

#define  MENU                             1
#define  MENU_FILE                        2
#define  MENU_FILE_LOAD_PAL               3       /* callback function: cbm_LoadPal */
#define  MENU_FILE_SAVE_PAL               4       /* callback function: cbm_SavePal */
#define  MENU_FILE_SEPARATOR              5
#define  MENU_FILE_LOAD_TXT               6       /* callback function: cbm_LoadTxt */
#define  MENU_FILE_SAVE_TXT               7       /* callback function: cbm_SaveTxt */
#define  MENU_FILE_SET_TXT                8       /* callback function: cbm_SetTxt */
#define  MENU_FILE_SEPARATOR_2            9
#define  MENU_FILE_SAVE_PNG               10      /* callback function: cbm_SavePng */
#define  MENU_FILE_SAVE_REF_PNG           11      /* callback function: cbm_SaveRefPng */
#define  MENU_FILE_SEPARATOR_3            12
#define  MENU_FILE_QUIT                   13      /* callback function: cbm_Quit */
#define  MENU_EDIT                        14
#define  MENU_EDIT_COPY_CANVAS            15      /* callback function: cbm_CopyCanvas */
#define  MENU_EDIT_COPY_REF               16      /* callback function: cbm_CopyRef */
#define  MENU_HELP                        17
#define  MENU_HELP_QUICK_HELP             18      /* callback function: cbm_QuickHelp */
#define  MENU_HELP_WEBSITE                19      /* callback function: cbm_Website */
#define  MENU_HELP_ABOUT                  20      /* callback function: cbm_About */


	 /* Callback Prototypes: */

int  CVICALLBACK cb_256(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Canvas(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_CanvasRef(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Color(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_CopyToRef(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_FixHeight(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Last(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Panel(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Quit(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Reset(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Scaling(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_TxtFormat(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
void CVICALLBACK cbm_About(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_CopyCanvas(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_CopyRef(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_LoadPal(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_LoadTxt(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_QuickHelp(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_Quit(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_SavePal(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_SavePng(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_SaveRefPng(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_SaveTxt(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_SetTxt(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_Website(int menubar, int menuItem, void *callbackData, int panel);


#ifdef __cplusplus
	}
#endif