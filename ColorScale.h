///////////////////////////// ColorScale.h ////////////////////////////////////
#ifndef _COLOR_SCALE
#define _COLOR_SCALE

typedef unsigned long COLORREF;	// 0 to 0xFFFFFF for colors and 0xFFFFFFFF for alpha partial transparency

// Create a 0xRRGGBB from separate 8 bits colors - error checking
#define MakeRGB(r,g,b)    (                 ((r)&0xFF)<<16 | ((g)&0xFF)<<8 | ((b)&0xFF))
#define MakeRGBA(r,g,b,a) (((a)&0xFF)<<24 | ((r)&0xFF)<<16 | ((g)&0xFF)<<8 | ((b)&0xFF))

// Scaling
extern COLORREF ColorScaleRGB(const COLORREF Col1, const COLORREF Col2, const double Ratio);
extern COLORREF ColorScaleHSL(const COLORREF Col1, const COLORREF Col2, const double Ratio);

extern COLORREF ColorStepsRGB(const COLORREF Col1, const COLORREF Col2, const double Ratio, const int NbSteps);
extern COLORREF ColorStepsHSL(const COLORREF Col1, const COLORREF Col2, const double Ratio, const int NbSteps);

extern COLORREF ColorScaleRGB3(const COLORREF Col1, const COLORREF Col2, const COLORREF Col3,
					double Ratio1, double Ratio2, double Ratio3);
extern COLORREF ColorScaleHSL3(const COLORREF Col1, const COLORREF Col2, const COLORREF Col3,
					double Ratio1, double Ratio2, double Ratio3);

// Conversions
extern void 	RGBtoHLS(const COLORREF rgb, double *H, double *L, double *S );
extern COLORREF	HLStoRGB(const double H, const double L, const double S );


///////////////////////////////////////////////////////////////////////////////

#ifdef __clang__	// I don't know if there is a better way to detect compound statements capabilities
	#define NO_COMPOUND_STATEMENTS
#endif



///////////////////////////////////////////////////////////////////////////////
#ifdef NO_COMPOUND_STATEMENTS
	#pragma clang diagnostic push
	#pragma clang diagnostic ignored "-Wunused-variable"
	static double _HTemp=0, _LTemp=0, _STemp=0;
	#pragma clang diagnostic pop

	///////////////////////////////////////////////////////////////////////////////
	// Darker and lighter colors
	#define DARKER(c)	(RGBtoHLS((c), &_HTemp, &_LTemp, &_STemp), HLStoRGB(_HTemp, Max(0,_LTemp-.1), _STemp))
	#define DARKEST(c)	(RGBtoHLS((c), &_HTemp, &_LTemp, &_STemp), HLStoRGB(_HTemp, Max(0,_LTemp-.2), _STemp))
	#define LIGHTER(c)	(RGBtoHLS((c), &_HTemp, &_LTemp, &_STemp), HLStoRGB(_HTemp, Min(1,_LTemp+.1), _STemp))
	#define LIGHTEST(c)	(RGBtoHLS((c), &_HTemp, &_LTemp, &_STemp), HLStoRGB(_HTemp, Min(1,_LTemp+.2), _STemp))
	// f:0..1 for lighter color, 0..-1 for darker
	#define LIGHT(c,f)	(RGBtoHLS((c), &_HTemp, &_LTemp, &_STemp), HLStoRGB(_HTemp, Min(1,_LTemp+(f)), _STemp))

	// More or less saturated colors
	#define MORE_SATURATED(c)	(RGBtoHLS((c), &_HTemp, &_LTemp, &_STemp), HLStoRGB(_HTemp, _LTemp, Max(0,_STemp+.1)))
	#define MOST_SATURATED(c)	(RGBtoHLS((c), &_HTemp, &_LTemp, &_STemp), HLStoRGB(_HTemp, _LTemp, Max(0,_STemp+.2)))
	#define LESS_SATURATED(c)	(RGBtoHLS((c), &_HTemp, &_LTemp, &_STemp), HLStoRGB(_HTemp, _LTemp, Max(0,_STemp-.1)))
	#define LEAST_SATURATED(c)	(RGBtoHLS((c), &_HTemp, &_LTemp, &_STemp), HLStoRGB(_HTemp, _LTemp, Max(0,_STemp-.2)))
	// f:0..1 for more saturation, 0..-1 for less saturation
	#define SATURATION(c,f)		(RGBtoHLS((c), &_HTemp, &_LTemp, &_STemp), HLStoRGB(_HTemp, _LTemp, Max(0,_STemp+(f))))

	// More or less hued colors
	#define MORE_HUED(c)	(RGBtoHLS((c), &_HTemp, &_LTemp, &_STemp), HLStoRGB(Max(0,_HTemp+.1), _LTemp, _STemp))
	#define MOST_HUED(c)	(RGBtoHLS((c), &_HTemp, &_LTemp, &_STemp), HLStoRGB(Max(0,_HTemp+.2), _LTemp, _STemp))
	#define LESS_HUED(c)	(RGBtoHLS((c), &_HTemp, &_LTemp, &_STemp), HLStoRGB(Max(0,_HTemp-.1), _LTemp, _STemp))
	#define LEAST_HUED(c)	(RGBtoHLS((c), &_HTemp, &_LTemp, &_STemp), HLStoRGB(Max(0,_HTemp-.2), _LTemp, _STemp))
	// f:0..1 for more hue, 0..-1 for less hue
	#define HUE(c,f)		(RGBtoHLS((c), &_HTemp, &_LTemp, &_STemp), HLStoRGB(Max(0,_HTemp+(f)), _LTemp, _STemp))
#else
	///////////////////////////////////////////////////////////////////////////////
	// Darker and lighter colors
	#define DARKER(c)	({ double H=0, L=0, S=0; RGBtoHLS((c), &H, &L, &S); HLStoRGB(H, Max(0,L-.1), S); })
	#define DARKEST(c)	({ double H=0, L=0, S=0; RGBtoHLS((c), &H, &L, &S); HLStoRGB(H, Max(0,L-.2), S); })
	#define LIGHTER(c)	({ double H=0, L=0, S=0; RGBtoHLS((c), &H, &L, &S); HLStoRGB(H, Min(1,L+.1), S); })
	#define LIGHTEST(c)	({ double H=0, L=0, S=0; RGBtoHLS((c), &H, &L, &S); HLStoRGB(H, Min(1,L+.2), S); })
	// f:0..1 for lighter color, 0..-1 for darker
	#define LIGHT(c,f)	({ double H=0, L=0, S=0; RGBtoHLS((c), &H, &L, &S); HLStoRGB(H, Min(1,L+(f)), S); })

	// More or less saturated colors
	#define MORE_SATURATED(c)	({ double H=0, L=0, S=0; RGBtoHLS((c), &H, &L, &S); HLStoRGB(H, L, Max(0,S+.1)); })
	#define MOST_SATURATED(c)	({ double H=0, L=0, S=0; RGBtoHLS((c), &H, &L, &S); HLStoRGB(H, L, Max(0,S+.2)); })
	#define LESS_SATURATED(c)	({ double H=0, L=0, S=0; RGBtoHLS((c), &H, &L, &S); HLStoRGB(H, L, Max(0,S-.1)); })
	#define LEAST_SATURATED(c)	({ double H=0, L=0, S=0; RGBtoHLS((c), &H, &L, &S); HLStoRGB(H, L, Max(0,S-.2)); })
	// f:0..1 for more saturation, 0..-1 for less saturation
	#define SATURATION(c,f)		({ double H=0, L=0, S=0; RGBtoHLS((c), &H, &L, &S); HLStoRGB(H, L, Max(0,S+(f))); })

	// More or less hued colors
	#define MORE_HUED(c)	({ double H=0, L=0, S=0; RGBtoHLS((c), &H, &L, &S); HLStoRGB(Max(0,H+.1), L, S); })
	#define MOST_HUED(c)	({ double H=0, L=0, S=0; RGBtoHLS((c), &H, &L, &S); HLStoRGB(Max(0,H+.2), L, S); })
	#define LESS_HUED(c)	({ double H=0, L=0, S=0; RGBtoHLS((c), &H, &L, &S); HLStoRGB(Max(0,H-.1), L, S); })
	#define LEAST_HUED(c)	({ double H=0, L=0, S=0; RGBtoHLS((c), &H, &L, &S); HLStoRGB(Max(0,H-.2), L, S); })
	// f:0..1 for more hue, 0..-1 for less hue
	#define HUE(c,f)		({ double H=0, L=0, S=0; RGBtoHLS((c), &H, &L, &S); HLStoRGB(Max(0,H+(f)), L, S); })
#endif


///////////////////////////////////////////////////////////////////////////////
// Extra color definitions (in addition to what is found in userint.h)
#define VAL_ORANGE         0xFF8000

#define VAL_MID_RED        0xA00000L
#define VAL_MID_BLUE       0x0000A0L
#define VAL_MID_GREEN      0x00A000L
#define VAL_MID_CYAN       0x00A0A0L
#define VAL_MID_MAGENT     0xA000A0L
#define VAL_MID_YELLOW     0xA0A000L
#define VAL_MID_GRAY       0xA0A0A0L	// Same as VAL_GRAY

#define VAL_LT_RED         0xC00000L
#define VAL_LT_BLUE        0x0000C0L
#define VAL_LT_GREEN       0x00C000L
#define VAL_LT_CYAN        0x00C0C0L
#define VAL_LT_MAGENTA     0xC000C0L
#define VAL_LT_YELLOW      0xC0C000L

#endif
