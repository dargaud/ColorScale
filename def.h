#ifndef _DEF
#define _DEF

#ifndef NULL
	#define NULL 0
#endif

// Better than malloc and calloc. Use the following syntax:
// whatevertype *Pointer=Calloc(10000, whatevertype);
#define Malloc(type)   (type *)malloc(sizeof(type))
#define Calloc(n,type) (type *)calloc(n, sizeof(type))
#define Free(Ptr) { free(Ptr); Ptr=NULL; }

#ifndef TRUE
	#define TRUE 1
	#define FALSE 0
#endif

// Just an extra test line
#endif // _DEF
