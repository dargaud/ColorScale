/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
	extern "C" {
#endif

	 /* Panels and Controls: */

#define  PNL                              1       /* callback function: cb_Panel */
#define  PNL_DECORATION_END               2       /* control type: deco, callback function: (none) */
#define  PNL_COLOR_END                    3       /* control type: color, callback function: cb_ColorEnd */
#define  PNL_E_R                          4       /* control type: numeric, callback function: cb_ColorEnd */
#define  PNL_E_G                          5       /* control type: numeric, callback function: cb_ColorEnd */
#define  PNL_E_B                          6       /* control type: numeric, callback function: cb_ColorEnd */
#define  PNL_E_H                          7       /* control type: numeric, callback function: cb_ColorEnd */
#define  PNL_E_S                          8       /* control type: numeric, callback function: cb_ColorEnd */
#define  PNL_E_L                          9       /* control type: numeric, callback function: cb_ColorEnd */
#define  PNL_END_HEX                      10      /* control type: string, callback function: cb_ColorEnd */
#define  PNL_DEC_HEX                      11      /* control type: binary, callback function: cb_DecHex */
#define  PNL_PERCENT                      12      /* control type: scale, callback function: cb_Percent */
#define  PNL_RESULT_RGB                   13      /* control type: color, callback function: cb_Copy */
#define  PNL_RESULT_HEX_RGB               14      /* control type: string, callback function: cb_Copy */
#define  PNL_RESULT_HSL                   15      /* control type: color, callback function: cb_Copy */
#define  PNL_RESULT_HEX_HSL               16      /* control type: string, callback function: cb_Copy */
#define  PNL_DECORATION_START             17      /* control type: deco, callback function: (none) */
#define  PNL_COLOR_START                  18      /* control type: color, callback function: cb_ColorStart */
#define  PNL_S_R                          19      /* control type: numeric, callback function: cb_ColorStart */
#define  PNL_S_G                          20      /* control type: numeric, callback function: cb_ColorStart */
#define  PNL_S_B                          21      /* control type: numeric, callback function: cb_ColorStart */
#define  PNL_S_H                          22      /* control type: numeric, callback function: cb_ColorStart */
#define  PNL_S_S                          23      /* control type: numeric, callback function: cb_ColorStart */
#define  PNL_S_L                          24      /* control type: numeric, callback function: cb_ColorStart */
#define  PNL_START_HEX                    25      /* control type: string, callback function: cb_ColorStart */
#define  PNL_CANVAS_RGB                   26      /* control type: canvas, callback function: cb_Canvas */
#define  PNL_CANVAS_HSL                   27      /* control type: canvas, callback function: cb_Canvas */
#define  PNL_QUIT                         28      /* control type: command, callback function: cb_Quit */
#define  PNL_COPYRIGHT                    29      /* control type: textMsg, callback function: cb_About */


	 /* Control Arrays: */

		  /* (no control arrays in the resource file) */


	 /* Menu Bars, Menus, and Menu Items: */

#define  MENU                             1
#define  MENU_FILE                        2
#define  MENU_FILE_LOAD_PAL               3       /* callback function: cbm_LoadPal */
#define  MENU_FILE_SAVE_PAL               4       /* callback function: cbm_SavePal */
#define  MENU_FILE_SEPARATOR              5
#define  MENU_FILE_LOAD_TXT               6       /* callback function: cbm_LoadTxt */
#define  MENU_FILE_SAVE_TXT               7       /* callback function: cbm_SaveTxt */
#define  MENU_FILE_SET_TXT                8       /* callback function: cbm_SetTxt */
#define  MENU_FILE_SEPARATOR_2            9
#define  MENU_FILE_SAVE_PNG               10      /* callback function: cbm_SavePng */
#define  MENU_FILE_SAVE_REF_PNG           11      /* callback function: cbm_SaveRefPng */
#define  MENU_FILE_SEPARATOR_3            12
#define  MENU_FILE_QUIT                   13      /* callback function: cbm_Quit */
#define  MENU_EDIT                        14
#define  MENU_EDIT_COPY_CANVAS            15      /* callback function: cbm_CopyCanvas */
#define  MENU_EDIT_COPY_REF               16      /* callback function: cbm_CopyRef */
#define  MENU_HELP                        17
#define  MENU_HELP_QUICK_HELP             18      /* callback function: cbm_QuickHelp */
#define  MENU_HELP_WEBSITE                19      /* callback function: cbm_Website */
#define  MENU_HELP_ABOUT                  20      /* callback function: cbm_About */


	 /* Callback Prototypes: */

int  CVICALLBACK cb_About(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Canvas(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_ColorEnd(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_ColorStart(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Copy(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_DecHex(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Panel(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Percent(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Quit(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
void CVICALLBACK cbm_About(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_CopyCanvas(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_CopyRef(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_LoadPal(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_LoadTxt(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_QuickHelp(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_Quit(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_SavePal(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_SavePng(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_SaveRefPng(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_SaveTxt(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_SetTxt(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_Website(int menubar, int menuItem, void *callbackData, int panel);


#ifdef __cplusplus
	}
#endif