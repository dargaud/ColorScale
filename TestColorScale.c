#include <userint.h>
#include "TestColorScale.h"
#include <cvirte.h>    /* Needed if linking in external compiler; harmless otherwise */
#include "ColorScale.h"

static int pnl;

void TestCorresp( COLORREF RGB ) {
	static double H, L, S;
	static int rgb;
	RGBtoHLS( RGB, &H, &L, &S);
	rgb= HLStoRGB( H, L, S );
	SetCtrlAttribute(pnl, PNL_BEF1, ATTR_TEXT_BGCOLOR, RGB);
	SetCtrlAttribute(pnl, PNL_AFT1, ATTR_TEXT_BGCOLOR, rgb);
}

void TestScale(COLORREF Col1, COLORREF Col2) {
	COLORREF ColA, ColB;
	int i;
	#define M 0x100
	double r, p, pp=1.0/M;
	for (i=0; i<M; i++,r=(float)i/(M-1),p=(float)i/M) {
		ColA=ColorScale(Col1, Col2, r);
		ColB=ColorScale2(Col1, Col2, r);
		PlotRectangle (pnl, PNL_GRAPH, p, 0, p+pp, 1, ColA, ColA);
		PlotRectangle (pnl, PNL_GRAPH, p, 1, p+pp, 2, ColB, ColB);
	}
}

int main (int argc, char *argv[]) {
	
	if (InitCVIRTE (0, argv, 0) == 0)    /* Needed if linking in external compiler; harmless otherwise */
		return -1;    /* out of memory */
	if ((pnl = LoadPanel (0, "TestColorScale.uir", PNL)) < 0)
		return -1;
	DisplayPanel (pnl);
	
	CB_RGB1(pnl,0,EVENT_COMMIT,0,0,0);
	CB_RGB2(pnl,0,EVENT_COMMIT,0,0,0);
	
	RunUserInterface();
#if 0    /* formerly excluded lines */
	TestCorresp(0xFFFFFF);
	TestCorresp(0xFF0000);
	TestCorresp(0x808080);
	TestCorresp(0x123456);
	TestCorresp(0x0248FF);
	TestCorresp(0x000000);
#endif   /* formerly excluded lines */

//	TestScale(0x000000, 0xFFFFFF);
//	TestScale(0xFF0000, 0x00FF00);
//	TestScale(0x123456, 0x808080);
//	TestScale(0x123456, 0x0248FF);
	
	return 0;
}


COLORREF R1=0, G1=0, B1=0, 
		R2=0xFF, G2=0xFF, B2=0xFF, 
		RGB1=0, RGB2=0xFFFFFF, rgb1, rgb2;
double H1=0, L1=0, S1=0, H2=0, L2=1, S2=0;

int CVICALLBACK CB_RGB1 (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(pnl, PNL_R1, &R1);
			GetCtrlVal(pnl, PNL_G1, &G1);
			GetCtrlVal(pnl, PNL_B1, &B1);
			SetCtrlVal(pnl, PNL_RGB1, RGB1=R1<<16 | G1<<8 | B1);
			RGBtoHLS( RGB1, &H1, &L1, &S1);
			SetCtrlVal(pnl, PNL_H1, H1);
			SetCtrlVal(pnl, PNL_L1, L1);
			SetCtrlVal(pnl, PNL_S1, S1);
			rgb1= HLStoRGB( H1, L1, S1 );
			SetCtrlAttribute(pnl, PNL_BEF1, ATTR_TEXT_BGCOLOR, RGB1);
			SetCtrlAttribute(pnl, PNL_AFT1, ATTR_TEXT_BGCOLOR, rgb1);
			break;
	}
	return 0;
}

int CVICALLBACK CB_HSL1 (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(pnl, PNL_H1, &H1);
			GetCtrlVal(pnl, PNL_L1, &L1);
			GetCtrlVal(pnl, PNL_S1, &S1);
			RGB1 = HLStoRGB( H1, L1, S1 );
			R1=RGB1>>16;
			G1=(RGB1>>8) & 0xFF;
			B1=RGB1 & 0xFF;
			SetCtrlVal(pnl, PNL_RGB1, RGB1);
			SetCtrlVal(pnl, PNL_R1, R1);
			SetCtrlVal(pnl, PNL_G1, G1);
			SetCtrlVal(pnl, PNL_B1, B1);
			SetCtrlAttribute(pnl, PNL_BEF1, ATTR_TEXT_BGCOLOR, RGB1);
			SetCtrlAttribute(pnl, PNL_AFT1, ATTR_TEXT_BGCOLOR, RGB1);
			break;
	}
	return 0;
}

int CVICALLBACK CB_RGB2 (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(pnl, PNL_R2, &R2);
			GetCtrlVal(pnl, PNL_G2, &G2);
			GetCtrlVal(pnl, PNL_B2, &B2);
			SetCtrlVal(pnl, PNL_RGB2, RGB2=R2<<16 | G2<<8 | B2);
			RGBtoHLS( RGB2, &H2, &L2, &S2);
			SetCtrlVal(pnl, PNL_H2, H2);
			SetCtrlVal(pnl, PNL_L2, L2);
			SetCtrlVal(pnl, PNL_S2, S2);
			rgb2= HLStoRGB( H2, L2, S2 );
			SetCtrlAttribute(pnl, PNL_BEF2, ATTR_TEXT_BGCOLOR, RGB2);
			SetCtrlAttribute(pnl, PNL_AFT2, ATTR_TEXT_BGCOLOR, rgb2);
			break;
	}
	return 0;
}

int CVICALLBACK CB_HSL2 (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(pnl, PNL_H2, &H2);
			GetCtrlVal(pnl, PNL_L2, &L2);
			GetCtrlVal(pnl, PNL_S2, &S2);
			RGB2 = HLStoRGB( H2, L2, S2 );
			R2=RGB2>>16;
			G2=(RGB2>>8) & 0xFF;
			B2=RGB2 & 0xFF;
			SetCtrlVal(pnl, PNL_RGB2, RGB2);
			SetCtrlVal(pnl, PNL_R2, R2);
			SetCtrlVal(pnl, PNL_G2, G2);
			SetCtrlVal(pnl, PNL_B2, B2);
			SetCtrlAttribute(pnl, PNL_BEF2, ATTR_TEXT_BGCOLOR, RGB2);
			SetCtrlAttribute(pnl, PNL_AFT2, ATTR_TEXT_BGCOLOR, RGB2);
			break;
	}
	return 0;
}

int CVICALLBACK cb_Scale (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			TestScale(RGB1, RGB2);
			break;
	}
	return 0;
}

int CVICALLBACK cb_Quit (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			QuitUserInterface (0);
			break;
	}
	return 0;
}
