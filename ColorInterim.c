/////////////////////////////////////////////////////////////////////////////////////
// PROGRAM:   GenPalette.exe                                                       //
// COPYRIGHT: Guillaume Dargaud 1999-2002 - Freeware                               //
// PURPOSE:   Create palette files                                                 //
// INSTALL:   run SETUP. It will also install the LabWindows/CVI runtime engine.   //
// HELP:      available by right-clicking an item.                                 //
// TUTORIAL:  http://www.gdargaud.net/Hack/GenPalette.html                         //
/////////////////////////////////////////////////////////////////////////////////////

#define _VER_ "1.0"			// Version number
#define _WEB_ "http://www.gdargaud.net/Hack/ColorInterim.html"
#define _COPY_ "� 2004 Guillaume Dargaud"

#include <stdio.h>
#include <string.h>
#include <iso646.h>

#include <utility.h>
#include <userint.h>
#include <cvirte.h>    /* Needed if linking in external compiler; harmless otherwise */

#include "def.h"
#include "ColorScale.h"

#include "ColorInterim.h"


static int Pnl=0;

static COLORREF ColorStart=0x000000, ColorEnd=0xFFFFFF;
static double Ratio=0.5;


static void UpdateInterim(void) {
	int C;
	char Str[9];

	C=ColorScaleRGB(ColorStart, ColorEnd, Ratio);
	SetCtrlVal(Pnl, PNL_RESULT_RGB, C);
	sprintf(Str, "#%06X", C);
	SetCtrlVal(Pnl, PNL_RESULT_HEX_RGB, Str);

	C=ColorScaleHSL(ColorStart, ColorEnd, Ratio);
	SetCtrlVal(Pnl, PNL_RESULT_HSL, C);
	sprintf(Str, "#%06X", C);
	SetCtrlVal(Pnl, PNL_RESULT_HEX_HSL, Str);
}

static void GeneratePalette(COLORREF ColStart, COLORREF ColEnd) {
	int Height, Width, i, Hsl, File;
	Point P1, P2;

	GetCtrlAttribute (Pnl, PNL_CANVAS_RGB, ATTR_HEIGHT, &Height);
	GetCtrlAttribute (Pnl, PNL_CANVAS_RGB, ATTR_WIDTH, &Width);

	P1.x=0;	P2.x=Width-1;
	CanvasStartBatchDraw (Pnl, PNL_CANVAS_RGB);
	CanvasStartBatchDraw (Pnl, PNL_CANVAS_HSL);
	for (i=0; i<Height; i++) {
		P1.y=P2.y=i;
		SetCtrlAttribute(Pnl, PNL_CANVAS_RGB, ATTR_PEN_COLOR,
			ColorScaleRGB(ColEnd, ColStart, (double)i/(Height-1)) );
		SetCtrlAttribute(Pnl, PNL_CANVAS_HSL, ATTR_PEN_COLOR,
			ColorScaleHSL(ColEnd, ColStart, (double)i/(Height-1)) );
		CanvasDrawLine (Pnl, PNL_CANVAS_RGB, P1, P2);
		CanvasDrawLine (Pnl, PNL_CANVAS_HSL, P1, P2);
	}
	CanvasEndBatchDraw (Pnl, PNL_CANVAS_RGB);
	CanvasEndBatchDraw (Pnl, PNL_CANVAS_HSL);
}


///////////////////////////////////////////////////////////////////////////////
int main (int argc, char *argv[]) {
	if (InitCVIRTE (0, argv, 0) == 0)	/* Needed if linking in external compiler; harmless otherwise */
		return -1;	/* out of memory */
	if ((Pnl = LoadPanel (0, "ColorInterim.uir", PNL)) < 0 ) {
		MessagePopup("Error", "File GenPalette.uir missing or corrupt !");
		return -1;
	}
	DisplayPanel (Pnl);
	cb_Panel(Pnl, EVENT_PANEL_SIZE, NULL, 0, 0);

	RunUserInterface ();

	return 0;
}


///////////////////////////////////////////////////////////////////////////////

int CVICALLBACK cb_Quit (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			QuitUserInterface (0);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Quit", "What did you seriously expect to see here ?!?");
			break;
	}
	return 0;
}

int CVICALLBACK cb_Percent (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int Top, Height;
	switch (event) {
		case EVENT_VAL_CHANGED:
		case EVENT_COMMIT:
			GetCtrlVal(panel, control, &Ratio);
			Ratio/=100.;
			UpdateInterim();
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Percent",
						"0% to select the Start color,\n"
						"100% for the End color\n"
						"or use any interpolated color in between.");
			break;
	}
	return 0;
}



int CVICALLBACK cb_Canvas (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int Top, Height, Modifiers;
	char Str[9];
	Point P={0,0};

	switch (event) {
		case EVENT_LEFT_CLICK:
		case EVENT_COMMIT:
			GetCtrlAttribute (Pnl, control, ATTR_TOP, &Top);
			GetCtrlAttribute (Pnl, control, ATTR_HEIGHT, &Height);
			GetGlobalMouseState (NULL, NULL, NULL, NULL, NULL, &Modifiers);
			if (Modifiers & VAL_SHIFT_MODIFIER) {
				P.y=eventData1-Top;
				CanvasGetPixel (Pnl, control, P, &ColorStart);
				SetCtrlVal(Pnl, PNL_COLOR_START, ColorStart);
				cb_ColorEnd (Pnl, PNL_COLOR_START, EVENT_COMMIT, NULL, 0, 0);
			} else if (Modifiers & VAL_MENUKEY_MODIFIER) {
				P.y=eventData1-Top;
				CanvasGetPixel (Pnl, control, P, &ColorEnd);
				SetCtrlVal(Pnl, PNL_COLOR_END, ColorEnd);
				cb_ColorEnd (Pnl, PNL_COLOR_END, EVENT_COMMIT, NULL, 0, 0);
			} else {
				Ratio=1.-(double)(eventData1-Top)/Height;	// -1 ???
				SetCtrlVal(Pnl, PNL_PERCENT, Ratio*100.);
				UpdateInterim();
				GetCtrlVal(Pnl, control==PNL_CANVAS_RGB ? PNL_RESULT_HEX_RGB : PNL_RESULT_HEX_HSL, Str);
				ClipboardPutText (Str);
			}
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Canvas",
						 "This is the interpolation between the Start and End colors.\n"
						 "Left canvas shows RGB interpolation,\n"
						 "while the right one contains HSL interpolation.\n"
						 "Click to select the interim color.\n"
						 "Shift-click to replace the Start color.\n"
						 "Ctrl-click to replace the End color.");
			break;
	}
	return 0;
}

int CVICALLBACK cb_Panel (int panel, int event, void *callbackData,
		int eventData1, int eventData2) {
	int PnlWidth, PnlHeight, Top, Left, CtrlHeight, CtrlWidth, CanW, CtrlTop,
		LabelTop, LabelLeft, DigTop, DigLeft;
	switch (event) {
		case EVENT_CLOSE:
			QuitUserInterface (0);
			break;

		case EVENT_PANEL_SIZE:
			GetPanelAttribute (Pnl, ATTR_WIDTH,  &PnlWidth);
			GetPanelAttribute (Pnl, ATTR_HEIGHT, &PnlHeight);
			if (PnlWidth <230) PnlWidth=230;
			if (PnlHeight<350) PnlHeight=350;
			SetPanelSize (Pnl, PnlHeight, PnlWidth);

			GetCtrlAttribute(Pnl, PNL_CANVAS_RGB, ATTR_LEFT, &Left);
			SetCtrlAttribute(Pnl, PNL_CANVAS_RGB, ATTR_WIDTH, CanW=(PnlWidth-Left)/2);
			SetCtrlAttribute(Pnl, PNL_CANVAS_HSL, ATTR_WIDTH, CanW-1);
			SetCtrlAttribute(Pnl, PNL_CANVAS_HSL, ATTR_LEFT,  Left+CanW+1);

			GetCtrlAttribute(Pnl, PNL_COPYRIGHT, ATTR_HEIGHT, &CtrlHeight);
			SetCtrlAttribute(Pnl, PNL_COPYRIGHT, ATTR_TOP,  CtrlTop=PnlHeight-CtrlHeight);
			GetCtrlAttribute(Pnl, PNL_CANVAS_RGB, ATTR_TOP, &Top);
			SetCtrlAttribute(Pnl, PNL_CANVAS_RGB, ATTR_HEIGHT, CtrlTop-Top);
			SetCtrlAttribute(Pnl, PNL_CANVAS_HSL, ATTR_HEIGHT, CtrlTop-Top);


			GetCtrlAttribute (Pnl, PNL_PERCENT, ATTR_LABEL_TOP,  &LabelTop);
			GetCtrlAttribute (Pnl, PNL_PERCENT, ATTR_LABEL_LEFT, &LabelLeft);
			GetCtrlAttribute (Pnl, PNL_PERCENT, ATTR_DIG_DISP_TOP, &DigTop);
			GetCtrlAttribute (Pnl, PNL_PERCENT, ATTR_DIG_DISP_LEFT, &DigLeft);

			SetCtrlAttribute (Pnl, PNL_PERCENT, ATTR_TOP,  Top);
			SetCtrlAttribute (Pnl, PNL_PERCENT, ATTR_HEIGHT, CtrlTop-Top);
			GetCtrlAttribute (Pnl, PNL_PERCENT, ATTR_WIDTH, &CtrlWidth);
			SetCtrlAttribute (Pnl, PNL_PERCENT, ATTR_LEFT, Left+CanW-CtrlWidth/2);

			SetCtrlAttribute (Pnl, PNL_PERCENT, ATTR_LABEL_TOP,  LabelTop);
			SetCtrlAttribute (Pnl, PNL_PERCENT, ATTR_LABEL_LEFT, LabelLeft);
			SetCtrlAttribute (Pnl, PNL_PERCENT, ATTR_DIG_DISP_TOP, DigTop);
			SetCtrlAttribute (Pnl, PNL_PERCENT, ATTR_DIG_DISP_LEFT, DigLeft);

			GeneratePalette(ColorStart, ColorEnd);
			break;
	}
	return 0;
}


///////////////////////////////////////////////////////////////////////////////
int  CVICALLBACK cb_About(int panel, int control, int event, void *callbackData,
		int eventData1, int eventData2) {
	switch (event) {
		case EVENT_LEFT_CLICK:
			MessagePopup("About ColorInterim",
						"ColorInterim.exe version " _VER_ "\n"
						"Freeware "_COPY_"\n"
						"Last compiled on " __DATE__ "\n"
						"More information and tutorial at "_WEB_);
			LaunchExecutable ("EXPLORER.EXE "_WEB_);
			break;
	}
	return 0;
}


///////////////////////////////////////////////////////////////////////////////

int CVICALLBACK cb_DecHex (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int I, F;
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(Pnl, PNL_DEC_HEX, &I);
			F = I ? VAL_HEX_FORMAT : VAL_DECIMAL_FORMAT;

			SetCtrlAttribute (Pnl, PNL_S_R, ATTR_SHOW_RADIX, I);
			SetCtrlAttribute (Pnl, PNL_S_R, ATTR_FORMAT, F);
			SetCtrlAttribute (Pnl, PNL_S_G, ATTR_SHOW_RADIX, I);
			SetCtrlAttribute (Pnl, PNL_S_G, ATTR_FORMAT, F);
			SetCtrlAttribute (Pnl, PNL_S_B, ATTR_SHOW_RADIX, I);
			SetCtrlAttribute (Pnl, PNL_S_B, ATTR_FORMAT, F);
			SetCtrlAttribute (Pnl, PNL_S_H, ATTR_SHOW_RADIX, I);
			SetCtrlAttribute (Pnl, PNL_S_H, ATTR_FORMAT, F);
			SetCtrlAttribute (Pnl, PNL_S_S, ATTR_SHOW_RADIX, I);
			SetCtrlAttribute (Pnl, PNL_S_S, ATTR_FORMAT, F);
			SetCtrlAttribute (Pnl, PNL_S_L, ATTR_SHOW_RADIX, I);
			SetCtrlAttribute (Pnl, PNL_S_L, ATTR_FORMAT, F);

			SetCtrlAttribute (Pnl, PNL_E_R, ATTR_SHOW_RADIX, I);
			SetCtrlAttribute (Pnl, PNL_E_R, ATTR_FORMAT, F);
			SetCtrlAttribute (Pnl, PNL_E_G, ATTR_SHOW_RADIX, I);
			SetCtrlAttribute (Pnl, PNL_E_G, ATTR_FORMAT, F);
			SetCtrlAttribute (Pnl, PNL_E_B, ATTR_SHOW_RADIX, I);
			SetCtrlAttribute (Pnl, PNL_E_B, ATTR_FORMAT, F);
			SetCtrlAttribute (Pnl, PNL_E_H, ATTR_SHOW_RADIX, I);
			SetCtrlAttribute (Pnl, PNL_E_H, ATTR_FORMAT, F);
			SetCtrlAttribute (Pnl, PNL_E_S, ATTR_SHOW_RADIX, I);
			SetCtrlAttribute (Pnl, PNL_E_S, ATTR_FORMAT, F);
			SetCtrlAttribute (Pnl, PNL_E_L, ATTR_SHOW_RADIX, I);
			SetCtrlAttribute (Pnl, PNL_E_L, ATTR_FORMAT, F);

			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: DecHex",
						"Select Hexadecimal (0..FF) or Decimal (0..255)\n"
						"display for R, G, B, H, S and L values.");
			break;
	}
	return 0;
}

int CVICALLBACK cb_ColorStart (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	char Str[9]="", *Ptr;
	int R, G, B, H, S, L;
	double HH, SS, LL;

	switch (event) {
		case EVENT_COMMIT:
		case EVENT_VAL_CHANGED:

			switch (control) {
				case PNL_COLOR_START:
								GetCtrlVal(panel, control, &ColorStart); break;
				case PNL_S_R:
				case PNL_S_G:
				case PNL_S_B:	GetCtrlVal(Pnl, PNL_S_R, &R);
								GetCtrlVal(Pnl, PNL_S_G, &G);
								GetCtrlVal(Pnl, PNL_S_B, &B);
								ColorStart = MakeRGB (R, G, B);
								break;
				case PNL_S_H:
				case PNL_S_S:
				case PNL_S_L:	GetCtrlVal(Pnl, PNL_S_H, &H);
								GetCtrlVal(Pnl, PNL_S_S, &S);
								GetCtrlVal(Pnl, PNL_S_L, &L);
								ColorStart = HLStoRGB (H/255., L/255., S/255.);
								break;
				case PNL_START_HEX:
					if (event!=EVENT_COMMIT) return 0;
					GetCtrlVal(panel, control, Str);
					Ptr = strpbrk(Str, "0123456789abcdefABCDEF");
					ColorStart = (Ptr==NULL ? 0 : strtoul(Ptr, NULL, 16));	// Form FFFFFF
					break;
			}

			// Updates all controls
			SetCtrlVal(Pnl, PNL_COLOR_START, ColorStart);
			SetCtrlVal(Pnl, PNL_S_R, (ColorStart&0xFF0000)>>16);
			SetCtrlVal(Pnl, PNL_S_G, (ColorStart&0x00FF00)>>8);
			SetCtrlVal(Pnl, PNL_S_B, (ColorStart&0x0000FF));
			if (control!=PNL_S_H and control!=PNL_S_S and control!=PNL_S_L) {
				RGBtoHLS (ColorStart, &HH, &LL, &SS);
				SetCtrlVal(Pnl, PNL_S_H, (int)(HH*255));
				SetCtrlVal(Pnl, PNL_S_S, (int)(SS*255));
				SetCtrlVal(Pnl, PNL_S_L, (int)(LL*255));
			}

			sprintf(Str, "#%06X", ColorStart);
			SetCtrlVal(Pnl, PNL_START_HEX, Str);

			GeneratePalette(ColorStart, ColorEnd);
			UpdateInterim();
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Start Color",
						"Select start color for the interpolation.");
			break;
	}
	return 0;
}

int CVICALLBACK cb_ColorEnd (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	char Str[9]="", *Ptr;
	int R, G, B, H, S, L;
	double HH, SS, LL;

	switch (event) {
		case EVENT_COMMIT:
		case EVENT_VAL_CHANGED:

			switch (control) {
				case PNL_COLOR_END:
							GetCtrlVal(panel, control, &ColorEnd); break;
				case PNL_E_R:
				case PNL_E_G:
				case PNL_E_B:	GetCtrlVal(Pnl, PNL_E_R, &R);
								GetCtrlVal(Pnl, PNL_E_G, &G);
								GetCtrlVal(Pnl, PNL_E_B, &B);
								ColorEnd = MakeRGB (R, G, B);
								break;
				case PNL_E_H:
				case PNL_E_S:
				case PNL_E_L:	GetCtrlVal(Pnl, PNL_E_H, &H);
								GetCtrlVal(Pnl, PNL_E_S, &S);
								GetCtrlVal(Pnl, PNL_E_L, &L);
								ColorEnd = HLStoRGB (H/255., L/255., S/255.);
								break;
				case PNL_END_HEX:
					if (event!=EVENT_COMMIT) return 0;
					GetCtrlVal(panel, control, Str);
					Ptr = strpbrk(Str, "0123456789abcdefABCDEF");
					ColorEnd = (Ptr==NULL ? 0 : strtoul(Ptr, NULL, 16));	// Form FFFFFF
					break;
			}

			// Updates all controls
			SetCtrlVal(Pnl, PNL_COLOR_END, ColorEnd);
			SetCtrlVal(Pnl, PNL_E_R, (ColorEnd&0xFF0000)>>16);
			SetCtrlVal(Pnl, PNL_E_G, (ColorEnd&0x00FF00)>>8);
			SetCtrlVal(Pnl, PNL_E_B, (ColorEnd&0x0000FF));
			if (control!=PNL_E_H and control!=PNL_E_S and control!=PNL_E_L) {
				RGBtoHLS (ColorEnd, &HH, &LL, &SS);
				SetCtrlVal(Pnl, PNL_E_H, (int)(HH*255));
				SetCtrlVal(Pnl, PNL_E_S, (int)(SS*255));
				SetCtrlVal(Pnl, PNL_E_L, (int)(LL*255));
			}

			sprintf(Str, "#%06X", ColorEnd);
			SetCtrlVal(Pnl, PNL_END_HEX, Str);

			GeneratePalette(ColorStart, ColorEnd);
			UpdateInterim();
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: end Color",
						"Select end color for the interpolation.");
			break;
	}
	return 0;
}

int CVICALLBACK cb_Copy (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	char Str[9];
	switch (event) {
		case EVENT_LEFT_CLICK:
			switch (control) {
				case PNL_RESULT_RGB:
				case PNL_RESULT_HEX_RGB:
					GetCtrlVal(Pnl, PNL_RESULT_HEX_RGB, Str); break;
				case PNL_RESULT_HSL:
				case PNL_RESULT_HEX_HSL:
					GetCtrlVal(Pnl, PNL_RESULT_HEX_HSL, Str); break;
			}
			ClipboardPutText (Str);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: intermediate color",
						"Those are the intermediate colors,\n"
						"as interpolated either with RGB or HSL interpolation.");
			break;
	}
	return 0;
}
